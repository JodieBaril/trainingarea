﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class DataAccessLayer2

    Public strCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("aeConn").ConnectionString

    Public Function getDataSet(sp_name As String, ParamNames() As String, ParamValues As String()) As DataSet
        Dim myConnection As New SqlConnection(strCon)
        myConnection.Open()
        Dim myCommand As New SqlCommand(sp_name, myConnection)
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.CommandTimeout = 0
        Dim ctr As Integer = 0
        For Each pName As String In ParamNames
            If pName <> "DUMMY" Then
                myCommand.Parameters.AddWithValue(pName, ParamValues(ctr))
            End If
            ctr = ctr + 1
        Next
        Dim dst As New DataSet()
        Dim mAdapter As New SqlDataAdapter(myCommand)
        mAdapter.Fill(dst)
        myConnection.Close()
        Return dst
    End Function

End Class
