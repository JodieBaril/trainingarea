﻿Imports System.Data
Imports DevExpress.Web

Partial Class _login
    Inherits System.Web.UI.Page

    Dim dal As DataAccessLayer2 = New DataAccessLayer2()

    Protected Sub LoginButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LoginButton.Click
        Dim strAgent As String = Request.UserAgent
        Dim os As String = ""
        ''#Windows OS's
        'If InStr(strAgent, "Windows NT 6.3") Then : os = "Windows 8.1"
        'ElseIf InStr(strAgent, "Windows NT 6.2") Then : os = "Windows 8"
        'ElseIf InStr(strAgent, "Windows NT 6.1") Then : os = "Windows 7"
        'ElseIf InStr(strAgent, "Windows NT 6.0") Then : os = "Windows Vista"
        'ElseIf InStr(strAgent, "Windows NT 5.2") Then : os = "Windows Server 2003"
        'ElseIf InStr(strAgent, "Windows NT 5.1") Then : os = "Windows XP"
        'ElseIf InStr(strAgent, "Windows NT 5.0") Then : os = "Windows 2000"
        'ElseIf InStr(strAgent, "Windows 98") Then : os = "Windows 98"
        'ElseIf InStr(strAgent, "Windows 95") Then : os = "Windows 95"
        '    ''#Mac OS's
        'ElseIf InStr(strAgent, "Mac OS") Then : os = "Mac OS"
        '    ''#Linux OS's
        'ElseIf InStr(strAgent, "Linux") Then : os = "Linux"
        'Else : os = strAgent
        'End If
        If txtuser.Text = "" Or TBPassword.Text = "" Then
            ErrorText.Text = "Please supply username and password."
            'Session("user") = ""
            'Session("userinits") = ""
            'Session("groupName") = ""
            'Session("groupID") = ""
            'Session("fullName") = ""
        Else
            ErrorText.Text = ""
            If Page.IsValid Then
                'Dim ds As DataSet = dal.getDataSet("usp_Login2", {"@username", "@password"}, {txtuser.Text, TBPassword.Text})
                If txtuser.Text.ToLower = "vtraining" And TBPassword.Text = "autoexam1" Then
                    'If Convert.ToString(ds.Tables(0).Rows(0)(0)) = "OK" Then
                    'Dim ds2 As DataSet = dal.getDataSet("usp_reportAudit", {"@username", "@ip", "@page", "@browser", "@os"}, {txtuser.Text, Request.UserHostAddress, "successful login", Request.Browser.Browser & " " & Request.Browser.Version, os})
                    'Session("eid") = Convert.ToString(ds.Tables(0).Rows(0)(1))
                    'Session("user") = txtuser.Text
                    'Session("userinits") = txtuser.Text
                    'Session("groupName") = Convert.ToString(ds.Tables(0).Rows(0)(3))
                    'Session("groupID") = Convert.ToString(ds.Tables(0).Rows(0)(4))
                    'Session("fullName") = Convert.ToString(ds.Tables(0).Rows(0)(5))
                    Session("isLO") = False
                    FormsAuthentication.RedirectFromLoginPage(txtuser.Text, False)
                Else
                    Dim ds As DataSet = dal.getDataSet("usp_getlogininfo", {"@username", "@password"}, {txtuser.Text.ToUpper, TBPassword.Text})
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("company") = Convert.ToString(ds.Tables(0).Rows(0)("remit_net"))
                        Session("companyid") = Convert.ToString(ds.Tables(0).Rows(0)("masterid"))
                        Session("user") = txtuser.Text.ToUpper
                        Session("branch") = Convert.ToString(ds.Tables(0).Rows(0)("branch"))
                        Session("lh") = Convert.ToString(ds.Tables(0).Rows(0)("masterid"))
                        Session("lhname") = Convert.ToString(ds.Tables(0).Rows(0)("lhname"))
                        Session("loemail") = Convert.ToString(ds.Tables(0).Rows(0)("EMAIL"))
                        Session("lhfax") = Convert.ToString(ds.Tables(0).Rows(0)("lh_fax"))
                        Session("lofax") = Convert.ToString(ds.Tables(0).Rows(0)("FAX"))
                        Session("lo") = Convert.ToString(ds.Tables(0).Rows(0)("MANAGER"))
                        Dim sss As String = Session("lo")
                        Session("admin") = Convert.ToString(ds.Tables(0).Rows(0)("WEB_ADMIN"))
                        Session("isLO") = True
                        Session("lo") = ds.Tables(0).Rows(0)("rid").ToString
                        FormsAuthentication.RedirectFromLoginPage(txtuser.Text, False)
                    Else
                        ErrorText.Text = "Invalid login details."
                    End If
                    'Session("user") = ""
                    'Session("userinits") = ""
                    'Session("groupName") = ""
                    'Session("groupID") = ""
                    'Session("fullName") = ""
                End If
            Else
                ErrorText.Text = "Invalid login details."
                'Session("user") = ""
                'Session("userinits") = ""
                'Session("groupName") = ""
                'Session("groupID") = ""
                'Session("fullName") = ""
            End If
            End If
    End Sub

End Class

