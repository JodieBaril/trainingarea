﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Product.aspx.vb" Inherits="Application_Product" MasterPageFile="~/Application/Training.master" %>
<asp:Content ID="ContentPlaceHolder1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper margin-bot">
				<div class="col-3">
					<div class="indent">
						<h2>Product Training</h2>
						<p class="color-4">The Vision VSC program is a best in class warranty program designed to save your members money in case of a mechanical breakdown. You are that member's source of information and expertise. The more you learn about the product the easier the process becomes for both you and the member. The short video below will provide you with a working knowledge of the Vision Warranty Corp VSC program.</p>
						<div class="wrapper p2">
							<figure class="img-indent"><img width="300" src="images/redcar.JPG" alt="" /></figure>
							<div class="extra-wrap">
						        Highlights:<br><br>
                                Vehicles up to 11 Years Old and with up to 150,000 Miles are eligilible.<br><br>
                                Full Open Enrollment.<br><br>
                                Exclusionary Coverage up 100,000 Miles.                             
							</div>
						</div>
                        <p class="color-4" style="text-align:center;"><br/>
                            Intro Video
                            <img height="15" src="images/play.jpg">Play to Begin <br/><br/>
                            <video src="Intro.wmv" width="640" height="480" typeof="application/x-mplayer2" controls></video>
                            <br/> 
                            Now that you have completed the introductory video please review the following video on the product details.
                            <br/><br/>
                            Product Details Video
                            Press <img height="15" src="images/play.jpg">Play to Begin <br/><br/>                          
                            <video width="640" height="480" controls src="ProductVideoFinal.wmv"></video>  
						</p>
					</div>
				</div>
				<div class="col-4">
					<div class="block-news">
						<h3 class="color-4 p2">Coverages</h3>
						<ul class="list-2">
							<li><a target="_blank" href="level3.pdf">LEVEL.3THREE</a></li>
							<li><a target="_blank" href="level3.pdf">LEVEL.3THREE WRAP</a></li>
							<li><a target="_blank" href="level2.pdf">LEVEL.2TWO</a></li>
                            <li><a target="_blank" href="level1.pdf">LEVEL.1ONE</a></li>
						</ul>
                        <br/><br/>
                        <h3 class="color-4 p2">Chat</h3>
                        <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
                        <div id="ciXstc" style="z-index:100;position:absolute"></div>
                        <div id="scXstc" style="display:inline"></div>
                        <div id="sdXstc" style="display:none"></div>
                        <script type="text/javascript">
                            var seXstc = document.createElement("script");
                            seXstc.type = "text/javascript";
                            var seXstcs = (location.protocol.indexOf("https") == 0 ? "https" : "http") + "://image.providesupport.com/js/autoexam/safe-standard.js?ps_h=Xstc&ps_t=" + new Date().getTime();
                            setTimeout("seXstc.src=seXstcs;document.getElementById('sdXstc').appendChild(seXstc)", 1)
                        </script>
                        <noscript>
                            <div style="display:inline"><a href="http://www.providesupport.com?messenger=autoexam">Live Help Desk</a></div>
                        </noscript>
                        <!-- END ProvideSupport.com Graphics Chat Button Code -->
                        <br/><br/><br/>
                        <h3 class="color-4 p2">Success</h3>
                        Knowledge of the product will lead to success in presenting it to your members.<br/>
                        <img width="165" src="images/EmployeeTraining.jpg"><br/>
					</div>
				</div>
		    </div>
	    </div>
    </section>
</asp:Content>