﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Schedule.aspx.vb" Inherits="Application_Schedule" MasterPageFile="~/Application/Training.master" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="ContentPlaceHolder1" runat="server">
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper margin-bot">
				<div class="col-3">
					<div class="indent">
						<h2>Contact Our Trainer</h2>
						<fieldset>
							<label><span class="text-form">Name:</span>
                            <input name="name" type="text" /></label>
							<label><span class="text-form">Email:</span>
                            <input name="email" type="text" /></label>   
							<label><span class="text-form">Phone:</span>
                            <input name="phone" type="text" /></label>		
                            <div class="wrapper">
                                <div class="text-form">Comments:</div>
                                <textarea name="comments"  id="comments" rows="5" cols="20"></textarea>
                            </div>
                            <input type="hidden" value="contactAE" name="type" id="type" />
							<div class="buttons">
								<a class="button-2" href="#" onClick="document.getElementById('contact-form').reset()">Clear</a>
								<a class="button-2" href="#" onClick="document.getElementById('contact-form').submit()">Send</a>
							</div>									 
						</fieldset>						
					</div>
				</div>
				<div class="col-4">
					<div class="block-news">
						<h3 class="color-4 indent-bot2">Contacts</h3>
						<dl class="contact p3">
							<dt><span>Our Address:</span></dt>
							<dd>11449 Gulf Fwy</dd>
							<dt>Houston, TX 77034</dt>
							<dd><span>Telephone:</span>1-800-709-5792</dd>
						</dl>
					<h3 class="color-4 indent-bot2">Our Trainer</h3>
					<p><img width="160" src="images/tej.jpg"></p>
						<p class="text-1">Tej Gill - Trainer</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</asp:Content>