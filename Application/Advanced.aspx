﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Advanced.aspx.vb" Inherits="Application_Advanced" MasterPageFile="~/Application/Training.master" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="ContentPlaceHoler1" runat="server">
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper margin-bot">
				<div class="col-3">
					<div>
						<h2>Sales: Overcoming Objections</h2>
						<p class="color-4">We know our members very well so it makes sense that we can talk to them about our products very comfortably. Even with our ability to communicate effectively our members may from time to time disagree with our recommendation or misunderstand our offering. These situations are called objections and it is up to us to make sure we clear up any misunderstanding the member may have about our product, and find out why they may want to decline our offering. Many times an objection is just a natural response to product offer and that is why our job on overcoming objections is so important. We have to push past this and ask those questions that will lead us to protecting more members. </p>
						<div class="wrapper p2">
							<figure class="img-indent"><img width="275" src="images/people1.JPG" alt="" /></figure>
							<div class="extra-wrap">
								<p>
                                    Highlights:<br/><br/>
                                    Overcoming Member Objections.<br/><br/>
                                    Honing your Presentation.
								</p>
								<p>&nbsp;</p>
								<p>COMING SOON!<br/><br/></p>
                            </div>
						</div>
                        <p class="color-4">&nbsp;</p>
					</div>							
				</div>
				<div class="col-4">
					<div class="block-news">
						<h3 class="color-4 p2">Chat</h3>
                        <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
                        <div id="ciXstc" style="z-index:100;position:absolute"></div><div id="scXstc" style="display:inline"></div><div id="sdXstc" style="display:none"></div><script type="text/javascript">var seXstc=document.createElement("script");seXstc.type="text/javascript";var seXstcs=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/autoexam/safe-standard.js?ps_h=Xstc&ps_t="+new Date().getTime();setTimeout("seXstc.src=seXstcs;document.getElementById('sdXstc').appendChild(seXstc)",1)</script><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=autoexam">Live Help Desk</a></div></noscript>
                        <!-- END ProvideSupport.com Graphics Chat Button Code -->
                        <br/><br/><br/>
                        <h3 class="color-4 p2">Success</h3>
                        Knowledge of the product will lead to success in presenting it to your members.<br/>
                        <img width="165" src="images/EmployeeTraining.jpg"><br/>
					</div>
				</div>
			</div>
		</div>
	</section>
</asp:Content>