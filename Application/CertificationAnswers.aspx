﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CertificationAnswers.aspx.vb" Inherits="Application_CertificationAnswers" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Repeater ID="dlAnswers" runat="server" DataSourceID="dsAnswers">
                <HeaderTemplate>
                    Category: <span id="spanCategory" runat="server"></span><br /><br />
                </HeaderTemplate>
                <ItemTemplate>
                    Question #<%# Eval("rid") %>: <%#Eval("question") %><br />
                    <br />
                    <span id="choice1" runat="server"><asp:RadioButton ID="rb1" runat="server" Enabled="false" /><%# Eval("choice1") %><br /></span>
                    <span id="choice2" runat="server"><asp:RadioButton ID="rb2" runat="server" Enabled="false" /><%# Eval("choice2") %><br /></span>
                    <span id="choice3" runat="server"><asp:RadioButton ID="rb3" runat="server" Enabled="false" /><%# Eval("choice3") %><br /></span>
                    <span id="choice4" runat="server"><asp:RadioButton ID="rb4" runat="server" Enabled="false" /><%# Eval("choice4") %><br /></span>
                    <span id="multiChoice1" runat="server"><asp:CheckBox ID="cb1" runat="server" Enabled="false" /><%# Eval("choice1") %><br /></span>
                    <span id="multiChoice2" runat="server"><asp:CheckBox ID="cb2" runat="server" Enabled="false" /><%# Eval("choice2") %><br /></span>
                    <span id="multiChoice3" runat="server"><asp:CheckBox ID="cb3" runat="server" Enabled="false" /><%# Eval("choice3") %><br /></span>
                    <span id="multiChoice4" runat="server"><asp:CheckBox ID="cb4" runat="server" Enabled="false" /><%# Eval("choice4") %><br /></span>
                    <br /><br />
                    <span id="spanMulti" runat="server" visible="false"><%# Eval("multipleAnswers") %></span>
                    <span id="spanAnswer" runat="server" visible="false" ><%# Eval("answer") %></span>      
                     <span id="spanCategoryHidden" runat="server" visible="false"><%# Eval("category") %></span>
               </ItemTemplate>
            </asp:Repeater>
            <asp:SqlDataSource ID="dsAnswers" runat="server" ConnectionString="<%$ ConnectionStrings:aeConn %>" 
                SelectCommand="select * from loan_officer_cert_answers a join loan_officer_cert_questions q on q.rid = a.question_id and a.lo_rid=@lo_rid order by question_id " SelectCommandType="Text">
                <SelectParameters>
                    <asp:SessionParameter Name="lo_rid" SessionField="lo" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <script type = "text/javascript">
          function OnClose()
          {
            if(window.opener != null && !window.opener.closed) 
            {
               window.opener.HideModalDiv();
            }
          }
          window.onunload = OnClose;
        </script>
    </form>
</body>
</html>
