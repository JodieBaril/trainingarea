﻿
Partial Class Application_Sales
    Inherits System.Web.UI.Page

    Private Sub Application_Sales_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        CType(Me.Master, Application_Training).BodyClass = "page4"
        CType(Me.Master, Application_Training).displayFooterChat = False
        CType(Me.Master, Application_Training).Sales.Attributes("class") = "active"
    End Sub

End Class
