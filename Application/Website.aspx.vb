﻿
Partial Class Application_Website
    Inherits System.Web.UI.Page

    Private Sub Application_Website_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        CType(Me.Master, Application_Training).BodyClass = "page2"
        CType(Me.Master, Application_Training).displayFooterChat = False
        CType(Me.Master, Application_Training).Website.Attributes("class") = "active"
        If Session("isLO") = True Then
            divLOCert.Visible = True
        Else
            divLOCert.Visible = False
        End If
    End Sub

End Class
