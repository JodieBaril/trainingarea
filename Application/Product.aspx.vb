﻿
Partial Class Application_Product
    Inherits System.Web.UI.Page

    Private Sub Application_Product_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        CType(Me.Master, Application_Training).BodyClass = "page2"
        CType(Me.Master, Application_Training).displayFooterChat = False
        CType(Me.Master, Application_Training).Product.Attributes("class") = "active"
    End Sub

End Class
