﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Application_index" MasterPageFile="~/Application/Training.master" %>
<asp:Content ID="ContentPlaceHolder1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="row-3">
		<div class="slider-wrapper">
			<div class="slider">
			    <ul class="items">
			        <li><img src="images/slider-img1.jpg" alt="">
				        <strong class="banner">
					        <strong class="b1">training goal</strong>
					        <strong class="b2">Learn</strong>
					        <strong class="b3">Gain product knowledge<br> and build confidence.</strong>
				        </strong>
			        </li>
			        <li><img src="images/slider-img2.jpg" alt="">
				        <strong class="banner">
					        <strong class="b1">service contracts</strong>
					        <strong class="b2">Protect</strong>
					        <strong class="b3">Protect your members from costly<br>mechanical breakdowns.</strong>
				        </strong>
			        </li>
			        <li><img src="images/slider-img3.jpg" alt="">
				        <strong class="banner">
					        <strong class="b1">offer the product</strong>
					        <strong class="b2">Presentation</strong>
					        <strong class="b3">Offering a VSC to every member protects <br>both members and your credit union.</strong>
				        </strong>
			        </li>
			    </ul>
                <a class="prev" href="#">prev</a>
				<a class="next" href="#">prev</a>
			</div>
		</div>
	</div>
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper">
				<div class="col-3">
					<div>
						<h2>About This Site</h2>
						<p class="color-4 p1">This site is designed to provide you with product knowledge, answer questions, and give you easy access to materials. This website serves as a great tool to increase your knowledge on the warranty program and build the confidence that is needed when explaining our protection products to your members. We have built this training site to centralize all of the resources you will need to be well versed in the VSC program. We have some short videos you can watch and take the assessment to test your knowledge. Please take a moment to familiarize yourself with this site. We have some great features available like ordering materials, watching videos, requesting training and live chat.<strong></strong></p>
						<p><img src="images/peeps.jpg"></p>
                    </div>
			    </div>
				<div class="col-2">
					<div class="block-news">
						<h3 class="color-4 p2">Training Tools</h3>
                        <br/>
                        <div class="wrapper">
							<a class="button-2" href="schedule.aspx"><cufon class="cufon cufon-canvas" alt="Schedule" style="width: 75px; height: 19px; "><canvas width="92" height="20" style="width: 92px; height: 20px; top: 1px; left: -1px; "></canvas><cufontext> Schedule</cufontext></cufon><cufon class="cufon cufon-canvas" alt=" Training" style="width: 45px; height: 19px; "><canvas width="57" height="20" style="width: 57px; height: 20px; top: 1px; left: -1px; "></canvas><cufontext>Training</cufontext></cufon></a>
						</div>
                        <br/>
						<div class="wrapper">
							<a class="button-2" href="downloads.aspx"><cufon class="cufon cufon-canvas" alt="Downloads" style="width: 75px; height: 19px; "><canvas width="92" height="20" style="width: 92px; height: 20px; top: 1px; left: -1px; "></canvas><cufontext> Downloads</cufontext></cufon><cufon class="cufon cufon-canvas" alt=" Page" style="width: 45px; height: 19px; "><canvas width="57" height="20" style="width: 57px; height: 20px; top: 1px; left: -1px; "></canvas><cufontext>Page</cufontext></cufon></a>
                            <img width="250 "src="images/wrenches.png">
						</div>
					</div>
				</div>
                <div class="col-4">
                <div class="indent"></div>
			</div>
		</div>
        </div>
	</section>
    <script type="text/javascript">
        //carousel();
        $(function(){
            $('.slider')._TMS({
                prevBu: '.prev',
                nextBu: '.next',
                playBu: '.play',
                duration: 800,
                easing: 'easeOutQuad',
                preset: 'simpleFade',
                pagination: false,
                slideshow: 3000,
                numStatus: false,
                pauseOnHover: true,
                banners: true,
                waitBannerAnimation: false,
                bannerShow: function (banner) {
                    banner
                        .hide()
                        .fadeIn(500)
                },
                bannerHide: function (banner) {
                    banner
                        .show()
                        .fadeOut(500)
                }
            });
		})
    </script>
</asp:Content>