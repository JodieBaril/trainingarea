﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Certification.aspx.vb" Inherits="Application_Certification" MasterPageFile="~/Application/Training.master" %>
<asp:Content ContentPlaceHolderID="head" ID="head" runat="server">
    <script type="text/javascript">
        function showAnswers() {
            var popUpObj = window.open("CertificationAnswers.aspx", "ModalPopUp", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=600,height=600,left=400,top=100");
            popUpObj.focus();
            LoadModalDiv();
        }
        function LoadModalDiv() {
             alert('here');
           var bcgDiv = document.getElementById("divBackground");
            bcgDiv.style.display = "block";
        }
        function HideModalDiv() {
            var bcgDiv = document.getElementById("divBackground");
            bcgDiv.style.display = "none";
        }
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="ContentPlaceHolder1" runat="server">
    <div id = "divBackground" style="position: fixed; z-index: 999; height: 100%; width: 100%; top: 0; left:0; background-color: Black; filter: alpha(opacity=60); opacity: 0.6; -moz-opacity: 0.8;display:none"></div>
    <script type="text/javascript">
    </script>
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper margin-bot">
				<div class="col-3">
                    <div class="indent">
						<h2>Loan Officer Certification</h2>
                        <div id="divTest" style="visibility:visible;" >
                            <asp:FormView ID="fvQuestions" runat="server" DataSourceID="dsQuestions" AllowPaging="true" PagerSettings-Mode="Numeric" PagerSettings-Visible="false" >
                                <ItemTemplate>
                                    Category: <%# Eval("category") %><br /><br />
                                    Question #<%# Eval("rid") %>: <%#Eval("question") %><br />
                                    <br />
                                    <% If Session("multi") = False Then %>
                                    <span id="choice1" runat="server"><asp:RadioButton ID="rb1" runat="server" onclick="onRBClick(this);" /><%# Eval("choice1") %><br /></span>
                                    <span id="choice2" runat="server"><asp:RadioButton ID="rb2" runat="server" onclick="onRBClick(this);" /><%# Eval("choice2") %><br /></span>
                                    <span id="choice3" runat="server"><asp:RadioButton ID="rb3" runat="server" onclick="onRBClick(this);" /><%# Eval("choice3") %><br /></span>
                                    <span id="choice4" runat="server"><asp:RadioButton ID="rb4" runat="server" onclick="onRBClick(this);" /><%# Eval("choice4") %><br /></span>
                                    <% Else %>
                                    <span id="multiChoice1" runat="server"><asp:CheckBox ID="cb1" runat="server" /><%# Eval("choice1") %><br /></span>
                                    <span id="multiChoice2" runat="server"><asp:CheckBox ID="cb2" runat="server" /><%# Eval("choice2") %><br /></span>
                                    <span id="multiChoice3" runat="server"><asp:CheckBox ID="cb3" runat="server" /><%# Eval("choice3") %><br /></span>
                                    <span id="multiChoice4" runat="server"><asp:CheckBox ID="cb4" runat="server" /><%# Eval("choice4") %><br /></span>
                                    <% End If %>
                                    <br /><br />
                                    <span id="spanMulti" runat="server" visible="false"><%# Eval("multipleAnswers") %></span>
                                    <span id="spanQID" runat="server" visible="false" ><%# Eval("rid") %></span>     
                                    <span id="spanUserAnswer" runat="server" visible="false"><%# Eval("answer") %></span>
                                </ItemTemplate>
                            </asp:FormView>
                            <asp:Button ID="btnNext" runat="server" Text="Next" />
                            <span id="spanSpacer" runat="server" visible="false">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <asp:Button ID="btnBack" runat="server" Text="Back" Visible="false" />
                            <span id="spanResults" runat="server" visible="false">
                                <br /><br />
                                <asp:LinkButton ID="hlReview" runat="server" Text="Review Your Answers"></asp:LinkButton>
                            </span>
                            <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                            <asp:LinkButton ID="hlRetry" runat="server" Text="Retake Test" Visible="false"></asp:LinkButton>
                        </div>
                        <a href="#" onclick="javascript:this.style.visibility='hidden';divTest.style.visibility='visible';" id="aStart" runat="server" style="cursor:pointer;visibility:hidden;">Start Training</a>
                        <asp:SqlDataSource ID="dsQuestions" runat="server" ConnectionString="<%$ ConnectionStrings:aeConn %>" 
                            SelectCommand="select * from loan_officer_cert_questions q left join loan_officer_cert_answers a on q.rid=a.question_id and a.lo_rid=@lo_rid" SelectCommandType="Text"
                            UpdateCommand="update loan_officer_cert_answers set answer=@answer where question_id=@question_id and lo_rid=@lo_rid" UpdateCommandType="Text"
                            InsertCommand="insert into loan_officer_cert_answers (lo_rid,question_id,answer) values (@lo_rid,@question_id,@answer)" InsertCommandType="Text"
                            DeleteCommand="delete from loan_officer_cert_answers where lo_rid=@lo_rid">
                            <SelectParameters>
                                <asp:SessionParameter Name="lo_rid" SessionField="lo" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="answer" />
                                <asp:Parameter Name="question_id" />
                                <asp:SessionParameter Name="lo_rid" SessionField="lo" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="lo_rid" SessionField="lo" />
                                <asp:Parameter Name="question_id" />
                                <asp:Parameter Name="answer" />
                            </InsertParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="lo_rid" SessionField="lo" />
                            </DeleteParameters>
                        </asp:SqlDataSource>
                        <script type="text/javascript">
                            function onRBClick(rb) {
                                var div = document.getElementById("divTest");
                                var rbs = div.getElementsByTagName("input");
                                for (i = 0; i < rbs.length; i++) {
                                    if (rbs[i].type == 'radio'){
                                        if (rb != rbs[i]) {
                                            rbs[i].checked = false;
                                        }
                                    }
                                }                      
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>