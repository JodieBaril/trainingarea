﻿
Partial Class Application_Training
    Inherits System.Web.UI.MasterPage

    Public Property BodyClass As String
        Get
            Return BodyTag.ID
        End Get
        Set(value As String)
            BodyTag.ID = value
        End Set
    End Property

    Public Property BodyTag As HtmlGenericControl
        Get
            Return masterBody
        End Get
        Set(value As HtmlGenericControl)
            masterBody = value
        End Set
    End Property

    Public Property chat As HtmlGenericControl
        Get
            Return ChatBlock
        End Get
        Set(value As HtmlGenericControl)
            ChatBlock = value
        End Set
    End Property

    Public Property displayFooterChat As Boolean
        Get
            If ChatBlock.Attributes("display") = "block" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(value As Boolean)
            If value = True Then
                ChatBlock.Attributes.Add("display", "block")
                h4.InnerText = "Chat:"
            Else
                ChatBlock.Attributes.Add("display", "none")
                h4.InnerText = ""
            End If
        End Set
    End Property

    Public Property form As HtmlForm
        Get
            Return form1
        End Get
        Set(value As HtmlForm)
            form1 = value
        End Set
    End Property

    Public Property Home As HtmlAnchor
        Get
            Return aHome
        End Get
        Set(value As HtmlAnchor)
            aHome = value
        End Set
    End Property

    Public Property Product As HtmlAnchor
        Get
            Return aProduct
        End Get
        Set(value As HtmlAnchor)
            aProduct = value
        End Set
    End Property

    Public Property Website As HtmlAnchor
        Get
            Return aWebsite
        End Get
        Set(value As HtmlAnchor)
            aWebsite = value
        End Set
    End Property

    Public Property Sales As HtmlAnchor
        Get
            Return aSales
        End Get
        Set(value As HtmlAnchor)
            aSales = value
        End Set
    End Property

    Public Property Advanced As HtmlAnchor
        Get
            Return aAdvanced
        End Get
        Set(value As HtmlAnchor)
            aAdvanced = value
        End Set
    End Property

End Class

