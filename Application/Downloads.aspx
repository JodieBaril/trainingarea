﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Downloads.aspx.vb" Inherits="Application_Downloads" MasterPageFile="~/Application/Training.master" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="ContentPlaceHolder1" runat="server">
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="indent">
				<h2>Click On Thumbnail to Download</h2>
				<div class="wrapper indent-bot">
					<div class="col-3">
						<div class="wrapper">
							<figure class="img-indent4"><a target="_blank" href="TrainingHandoutVision.pdf"><img src="images/traininghandout.jpg" alt="" /></a></figure>
							<div class="extra-wrap">
								<h6>Training Guide</h6>
								This one page handout is the training handout that is used during our on-site training sessions. This document has most of the important information on this program.
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="wrapper">
							<figure class="img-indent3"><a target="_blank" href="brochureproof.pdf"><img src="images/brochure.jpg" alt="" /></a></figure>
							<div class="extra-wrap">
								<h6>Brochure</h6>
								This is a sample of our brochure.  This is for your reference.  If you need more brochure please contact Auto Exam directly for replacement supplies.
							</div>
						</div>
					</div>
				</div>
				<div class="wrapper indent-bot2">
					<div class="col-3">
						<div class="wrapper">
							<figure class="img-indent3"><a target="_blank" href="visionLaminate_proof.pdf"><img src="images/laminate.jpg" alt="" /></a></figure>
							<div class="extra-wrap">
								<h6>Laminate Guide</h6>
								This guide is designed to kept on the credit union employee's desk as a quick reference. The basic coverage information is included on the laminate guide.
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="wrapper">
							<figure class="img-indent"><a target="_blank" href="visionwarranty.pptx"><img height="96" src="images/visionwarranty.jpg" alt="" /></a></figure>
							<div class="extra-wrap">
								<h6>Training Presentation</h6>
								This presentation is used during our on-site trainings.  The powerpoint is here for you to revisit and review. 
							</div>
						</div>
					</div>
				</div>
				<div class="wrapper p3">
					<div class="col-4">
					<div class="wrapper">
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<figure class="img-indent"></figure>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
</asp:Content>