﻿
Partial Class Application_Certification
    Inherits System.Web.UI.Page

    Private Sub Application_Certification_Init(sender As Object, e As EventArgs) Handles Me.Init
        CType(Me.Master, Application_Training).BodyClass = "page2"
        CType(Me.Master, Application_Training).displayFooterChat = True
        CType(Me.Master, Application_Training).Website.Attributes("class") = "active"
    End Sub

    Private Sub Application_Certification_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            dsQuestions.Delete()
            fvQuestions.DataBind()
            CheckMulti()
        End If
    End Sub

    Private Sub CheckMulti()
        Dim multi As HtmlGenericControl = TryCast(fvQuestions.FindControl("spanMulti"), HtmlGenericControl)
        Session("multi") = False
        If Not IsNothing(multi) Then
            Dim show As Boolean = CBool(multi.InnerText)
            If show = True Then Session("multi") = True
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        lblErrorMsg.Text = ""
        spanResults.Visible = False
        If fvQuestions.PageIndex > 0 Then
            fvQuestions.PageIndex -= 1
            If btnNext.Text = "Get Results" Then btnNext.Text = "Next"
            fvQuestions.DataBind()
        End If
        If fvQuestions.PageIndex > 0 Then
            btnBack.Visible = True
            spanSpacer.Visible = True
        Else
            btnBack.Visible = False
            spanSpacer.Visible = False
        End If
        CheckMulti()
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        lblErrorMsg.Text = ""
        If ValidAnswers() = True Then
            If btnNext.Text = "Get Results" Then
                ScoreTest()
            Else
                SaveAnswer()
                If fvQuestions.PageIndex <= fvQuestions.PageCount - 2 Then
                    fvQuestions.PageIndex += 1
                    fvQuestions.DataBind()
                    Dim maxPage As Int16 = fvQuestions.PageCount - 1
                    If fvQuestions.PageIndex < maxPage - 1 Then
                        btnNext.Text = "Next"
                    ElseIf fvQuestions.PageIndex = maxPage Then
                        btnNext.Text = "Get Results"
                        spanResults.Visible = True
                    End If
                End If
                CheckMulti()
                If fvQuestions.PageIndex > 0 Then
                    btnBack.Visible = True
                    spanSpacer.Visible = True
                Else
                    btnBack.Visible = False
                    spanSpacer.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub fvQuestions_PageIndexChanged(sender As Object, e As EventArgs) Handles fvQuestions.PageIndexChanged
        CheckMulti()
    End Sub

    Private Function ValidAnswers() As Boolean
        Dim valid As Boolean = False
        Dim totalAnswers As Int16 = 0
        If Session("multi") = False Then
            If CType(fvQuestions.FindControl("rb1"), RadioButton).Checked Then totalAnswers += 1
            If CType(fvQuestions.FindControl("rb2"), RadioButton).Checked Then totalAnswers += 1
            If CType(fvQuestions.FindControl("rb3"), RadioButton).Checked Then totalAnswers += 1
            If CType(fvQuestions.FindControl("rb4"), RadioButton).Checked Then totalAnswers += 1
        Else
            If CType(fvQuestions.FindControl("cb1"), CheckBox).Checked Then totalAnswers += 1
                If CType(fvQuestions.FindControl("cb2"), CheckBox).Checked Then totalAnswers += 1
                If CType(fvQuestions.FindControl("cb3"), CheckBox).Checked Then totalAnswers += 1
                If CType(fvQuestions.FindControl("cb4"), CheckBox).Checked Then totalAnswers += 1
            End If
        If totalAnswers = 0 Then
            Session("script") = "<script>alert('You must select an answer first.');</script>"
            ClientScript.RegisterClientScriptBlock(Me.GetType, "script2", Session("script"))
            Session("script") = ""
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveAnswer()
        'dsQuestions.InsertParameters("lo_rid").DefaultValue = Session("lo")
        Dim qid As String = TryCast(fvQuestions.FindControl("spanQID"), HtmlGenericControl).InnerText
        dsQuestions.InsertParameters("question_id").DefaultValue = qid
        Dim answer As String = GetSelection()
        dsQuestions.InsertParameters("answer").DefaultValue = answer
        Try
            dsQuestions.Insert()
        Catch ex As Exception
            If ex.Message.ToUpper.Contains("VIOLATION OF PRIMARY KEY") Then
                dsQuestions.UpdateParameters("answer").DefaultValue = answer
                dsQuestions.UpdateParameters("question_id").DefaultValue = qid
                'dsQuestions.UpdateParameters("lo_rid").DefaultValue = Session("lo")
                Try
                    dsQuestions.Update()
                Catch exu As Exception
                    lblErrorMsg.Text = exu.Message
                End Try
            Else
                lblErrorMsg.Text = ex.Message
            End If
        End Try
    End Sub

    Private Function GetSelection() As String
        Dim answer As String = ""
        If Session("multi") = True Then
            If TryCast(fvQuestions.FindControl("cb1"), CheckBox).Checked Then answer = "0,"
            If TryCast(fvQuestions.FindControl("cb2"), CheckBox).Checked Then answer = answer & "1,"
            If TryCast(fvQuestions.FindControl("cb3"), CheckBox).Checked Then answer = answer & "2,"
            If TryCast(fvQuestions.FindControl("cb4"), CheckBox).Checked Then answer = answer & "3"
            If Right(answer, 1) = "," Then answer = Left(answer, answer.Length - 1)
        Else
            If TryCast(fvQuestions.FindControl("rb1"), RadioButton).Checked Then answer = 0
            If TryCast(fvQuestions.FindControl("rb2"), RadioButton).Checked Then answer = 1
            If TryCast(fvQuestions.FindControl("rb3"), RadioButton).Checked Then answer = 2
            If TryCast(fvQuestions.FindControl("rb4"), RadioButton).Checked Then answer = 3
        End If
        Return answer
    End Function

    Private Sub ScoreTest()
        Dim dal As New DataAccessLayer2
        Dim ds As System.Data.DataSet = dal.getDataSet("usp_score_loan_officer_cert", {"@lo_rid"}, {Session("lo")})
        fvQuestions.Visible = False
        btnNext.Visible = False
        btnBack.Visible = False
        spanSpacer.Visible = False
        spanResults.Visible = False
        Dim score As Double = ds.Tables(0).Rows(0)("score") * 100
        lblErrorMsg.Text = "You completed the test with a score of " & score & "."
        If ds.Tables(0).Rows(0)("pass_fail") = "1" Then
            lblErrorMsg.Text += " <br/>Congratulations! You have passed the test."
        Else
            lblErrorMsg.Text += " <br/>You did not achieve a passing score.<br/>Please try again another time.<br/><br/>"
            hlRetry.Visible = True
        End If
    End Sub

    Private Sub hlRetry_Click(sender As Object, e As EventArgs) Handles hlRetry.Click
        fvQuestions.Visible = True
        fvQuestions.PageIndex = 0
        fvQuestions.DataBind()
        CheckMulti()
        btnBack.Visible = False
        btnNext.Visible = True
        btnNext.Text = "Next"
        lblErrorMsg.Text = ""
        hlRetry.Visible = False
        spanResults.Visible = False
        dsQuestions.Delete()
    End Sub

    Private Sub hlReview_Click(sender As Object, e As EventArgs) Handles hlReview.Click
        If ValidAnswers() Then
            SaveAnswer()
            Session("script") = "<script>var popUpObj = window.open('CertificationAnswers.aspx', 'ModalPopUp', 'toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=600,height=600,left=400,top=100');</script>"
            ClientScript.RegisterClientScriptBlock(Me.GetType, "script2", Session("script"))
            Session("script") = ""
        End If
    End Sub

    Private Sub fvQuestions_DataBound(sender As Object, e As EventArgs) Handles fvQuestions.DataBound
        CheckMulti()
        Dim userAnswer As String = TryCast(fvQuestions.FindControl("spanUserAnswer"), HtmlGenericControl).InnerText 'Can't CInt here because multi answers with comma will just "drop" the comma causing 2,3 to be 23, which would cause an error on checked
        If userAnswer.Length > 0 Then
            Dim cb1 As CheckBox = TryCast(fvQuestions.FindControl("cb1"), CheckBox)
            Dim cb2 As CheckBox = TryCast(fvQuestions.FindControl("cb2"), CheckBox)
            Dim cb3 As CheckBox = TryCast(fvQuestions.FindControl("cb3"), CheckBox)
            Dim cb4 As CheckBox = TryCast(fvQuestions.FindControl("cb4"), CheckBox)
            Dim cbList() As CheckBox = {cb1, cb2, cb3, cb4}
            Dim rb1 As RadioButton = TryCast(fvQuestions.FindControl("rb1"), RadioButton)
            Dim rb2 As RadioButton = TryCast(fvQuestions.FindControl("rb2"), RadioButton)
            Dim rb3 As RadioButton = TryCast(fvQuestions.FindControl("rb3"), RadioButton)
            Dim rb4 As RadioButton = TryCast(fvQuestions.FindControl("rb4"), RadioButton)
            Dim rbList() As RadioButton = {rb1, rb2, rb3, rb4}
            If Session("multi") = True And userAnswer.Length > 0 Then
                Dim answers() As String = userAnswer.ToString.Split(",")
                For Each answer In answers
                    cbList(CInt(answer)).Checked = True
                Next
            Else
                rbList(CInt(userAnswer)).Checked = True
            End If
        End If
    End Sub

End Class
