﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Sales.aspx.vb" Inherits="Application_Sales" MasterPageFile="~/Application/Training.master" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="ContentPlaceHolder1" runat="server">
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper margin-bot">
				<div class="col-3">
					<div>
						<h2>Sales Training</h2>
						<p class="color-4">As member advocate your job is to ensure that every members needs are taken care of, in addition to that we at the credit union level we have a few products we offer to our members. These products are designed to protect our members and provide them an affordable alternative from our dealer competitors. We must take the role of a consultant not a sales person because we “sell” for different reasons than other people. Our credit union employees “sell” our products to their members to ensure they are protected. We genuinely care for our members and that is why we offer every to product to every member every time.</p>
						<div class="wrapper p2">
							<figure class="img-indent"><img width="280" src="images/people2.JPG" alt="" /></figure>
							<div class="extra-wrap">
								<p>Highlights:<br/><br/>
                                    Learn to effectively offer the product.<br/><br/>
                                    Best sales practices.<br/><br/>
                                    Keys to your success.
								</p>
								<p>&nbsp;</p>
								<p>									</p>
							</div><br/>
                            <br/>
                            <div style="text-align:center;width:100%">
                                Sales Video
                                Press <img height="15" src="images/play.jpg">Play to Begin<br/><br/>
                                <video width="640" height="480" controls src="Sales.mp4" typeof="video/mp4"></video>
                                <br/>
                            </div>
						</div>
                        <p class="color-4">&nbsp;</p>
					</div>							
				</div>
				<div class="col-4">
					<div class="block-news">
						<h3 class="color-4 p2">Chat</h3>
                        <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
                        <div id="ciXstc" style="z-index:100;position:absolute"></div><div id="scXstc" style="display:inline"></div><div id="sdXstc" style="display:none"></div><script type="text/javascript">var seXstc=document.createElement("script");seXstc.type="text/javascript";var seXstcs=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/autoexam/safe-standard.js?ps_h=Xstc&ps_t="+new Date().getTime();setTimeout("seXstc.src=seXstcs;document.getElementById('sdXstc').appendChild(seXstc)",1)</script><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=autoexam">Live Help Desk</a></div></noscript>
                        <!-- END ProvideSupport.com Graphics Chat Button Code -->
                        <br/><br/><br/>
                        <h3 class="color-4 p2">Success</h3>
                        Knowledge of the product will lead to success in presenting it to your members.<br/>
                        <img width="165" src="images/EmployeeTraining.jpg"><br/>
					</div>
				</div>
			</div>
		</div>
	</section>
</asp:Content>