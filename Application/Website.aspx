﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Website.aspx.vb" Inherits="Application_Website" MasterPageFile="~/Application/Training.master" %>
<asp:Content ID="ContentPlaceHolder1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="ic"></div>
	<section id="content">
		<div class="padding">
			<div class="wrapper margin-bot">
				<div class="col-3">
					<div class="indent">
						<h2>Website Training</h2>
						<p class="color-4">In order to offer the product, it is important that you have a full understanding of our website and its capabilities.  This training module will teach you how to use our website and its features.  The website will not only allow you to quote the product and purchase the product, but will also give you valuable tools to offer the product.</p>
						<div class="wrapper p2">
							<figure class="img-indent"><img width="270" src="images/monitor.JPG" alt="" /></figure>
							<div class="extra-wrap">
								<p>Highlights:<br/><br/>
                                    Web-based Quoting.<br/><br/>
                                    Purchasing a Contract.<br/><br/>
                                    Online Sales Tools.
								</p>
								<p>&nbsp;</p>
								<p>COMING SOON!</p>
								<p>&nbsp;</p>
                                <div id="divLOCert" runat="server" class="wrapper-2">
								    <p>Available Training:</p>
                                    <a href="Certification.aspx">Loan Officer Certification</a>
                                </div>
							</div>
						</div>
                        <p class="color-4">&nbsp;</p>
					</div>
				</div>
				<div class="col-4">
					<div class="block-news">
						<h3 class="color-4 p2">Chat</h3>
                        <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
                        <div id="ciXstc" style="z-index:100;position:absolute"></div><div id="scXstc" style="display:inline"></div><div id="sdXstc" style="display:none"></div><script type="text/javascript">var seXstc=document.createElement("script");seXstc.type="text/javascript";var seXstcs=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/autoexam/safe-standard.js?ps_h=Xstc&ps_t="+new Date().getTime();setTimeout("seXstc.src=seXstcs;document.getElementById('sdXstc').appendChild(seXstc)",1)</script><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=autoexam">Live Help Desk</a></div></noscript>
                        <!-- END ProvideSupport.com Graphics Chat Button Code -->
                        <br/><br/><br/>
                        <h3 class="color-4 p2">Success</h3>
                        Knowledge of the product will lead to success in presenting it to your members.<br/>
                        <img width="165" src="images/EmployeeTraining.jpg"><br/>
					</div>
				</div>
			</div>
		</div>
	</section>
</asp:Content>