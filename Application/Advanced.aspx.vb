﻿
Partial Class Application_Advanced
    Inherits System.Web.UI.Page

    Private Sub Application_Advanced_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        CType(Me.Master, Application_Training).BodyClass = "page5"
        CType(Me.Master, Application_Training).displayFooterChat = False
        CType(Me.Master, Application_Training).Advanced.Attributes("class") = "active"
    End Sub

End Class
