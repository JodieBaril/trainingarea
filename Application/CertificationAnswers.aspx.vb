﻿
Partial Class Application_CertificationAnswers
    Inherits System.Web.UI.Page

    Private Sub dlAnswers_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles dlAnswers.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim spanCategory As HtmlGenericControl = TryCast(dlAnswers.Controls(0).Controls(0).FindControl("spanCategory"), HtmlGenericControl)
            spanCategory.InnerText = TryCast(e.Item.FindControl("spanCategoryHidden"), HtmlGenericControl).InnerText
            Dim userAnswer As String = TryCast(e.Item.FindControl("spanAnswer"), HtmlGenericControl).InnerText 'Can't CInt here because multi answers with comma will just "drop" the comma causing 2,3 to be 23, which would cause an error on checked
            Dim cb1 As CheckBox = TryCast(e.Item.FindControl("cb1"), CheckBox)
            Dim cb2 As CheckBox = TryCast(e.Item.FindControl("cb2"), CheckBox)
            Dim cb3 As CheckBox = TryCast(e.Item.FindControl("cb3"), CheckBox)
            Dim cb4 As CheckBox = TryCast(e.Item.FindControl("cb4"), CheckBox)
            Dim cbList() As CheckBox = {cb1, cb2, cb3, cb4}
            Dim rb1 As RadioButton = TryCast(e.Item.FindControl("rb1"), RadioButton)
            Dim rb2 As RadioButton = TryCast(e.Item.FindControl("rb2"), RadioButton)
            Dim rb3 As RadioButton = TryCast(e.Item.FindControl("rb3"), RadioButton)
            Dim rb4 As RadioButton = TryCast(e.Item.FindControl("rb4"), RadioButton)
            Dim rbList() As RadioButton = {rb1, rb2, rb3, rb4}
            Dim multi As HtmlGenericControl = TryCast(e.Item.FindControl("spanMulti"), HtmlGenericControl)
            If Not IsNothing(multi) Then
                Dim showMulti As Boolean = CBool(multi.InnerText)
                e.Item.FindControl("choice1").Visible = Not showMulti
                e.Item.FindControl("choice2").Visible = Not showMulti
                e.Item.FindControl("choice3").Visible = Not showMulti
                e.Item.FindControl("choice4").Visible = Not showMulti
                e.Item.FindControl("multiChoice1").Visible = showMulti
                e.Item.FindControl("multiChoice2").Visible = showMulti
                e.Item.FindControl("multiChoice3").Visible = showMulti
                e.Item.FindControl("multiChoice4").Visible = showMulti
                If showMulti = True Then
                    Dim answers() As String = userAnswer.ToString.Split(",")
                    For Each answer In answers
                        cbList(CInt(answer)).Checked = True
                    Next
                Else
                    rbList(CInt(userAnswer)).Checked = True
                End If
            End If
        End If
    End Sub

End Class
