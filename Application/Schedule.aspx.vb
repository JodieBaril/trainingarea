﻿
Partial Class Application_Schedule
    Inherits System.Web.UI.Page

    Private Sub Application_Schedule_PreInit(sender As Object, e As EventArgs) Handles Me.PreInit
        CType(Me.Master, Application_Training).BodyClass = "page5"
        CType(Me.Master, Application_Training).form.enctype = "multipart/form-data"
        CType(Me.Master, Application_Training).form.action = "http://www.autoexam.com/aequotes/oecgi2.exe/inet_site_forms"
        CType(Me.Master, Application_Training).form.id = "contact-form"
        CType(Me.Master, Application_Training).displayFooterChat = True
        CType(Me.Master, Application_Training).Home.Attributes("class") = "active"
    End Sub

End Class
